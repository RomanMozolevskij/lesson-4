<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 06/05/2018
 * Time: 15:21
 */

namespace App\Repository;




use Doctrine\ORM\EntityRepository;


class LectureRepository extends EntityRepository
{

    public function createLectureQueryBuilder()
    {
        return $this->createQueryBuilder('lecture')
            ->orderBy('lecture.lectureName', 'ASC');
    }
}