<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 02/05/2018
 * Time: 13:06
 */

namespace App\Security;


use App\Form\LoginType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{

    private $formFactory;
    private $tulikas;
    private $router;
    private $passwordEncoder;

    public function __construct(FormFactoryInterface $formFactory, EntityManager $tulikas,
                                RouterInterface $router, UserPasswordEncoder $passwordEncoder)
    {

        $this->formFactory = $formFactory;
        $this->tulikas = $tulikas;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
    }




    public function getCredentials(Request $request)
    {
      $isLoginSubmit = $request->getPathInfo() == '/login' && $request->isMethod('POST');
      if(!$isLoginSubmit){
          return;
      }

      $form = $this->formFactory->create(LoginType::class);
      $form->handleRequest($request);

      $data = $form->getData();


      $request->getSession()->set(
          Security::LAST_USERNAME,
          $data['username']
        );

      return $data;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['username'];

        return $this->tulikas->getRepository('App:User')
            ->findOneBy(['email' => $username]);
    }



    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['password'];

        if($this->passwordEncoder->isPasswordValid($user, $password))
        {
            return true;
        }
       return false;
    }



    protected function getLoginUrl()
    {
      return $this->router->generate('login');
    }


    use TargetPathTrait;

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);

        if(!$targetPath) {
            $targetPath = $this->router->generate('user_show');
        }

        return new RedirectResponse($targetPath);
    }



    public function supports(Request $request)
    {

    }


}