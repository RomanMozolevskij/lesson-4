<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 06/05/2018
 * Time: 14:55
 */

namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity
 * @ORM\Table(name="articles")
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $articleName;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lecture", inversedBy="articles")
     */
    private $lecture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="articles")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="teachersActicles")
     */
    private $teacher_article;


    /**
     * @ORM\Column(type="integer")
     */
    private $views = 0;

/////////////////////////////////////<-getters & setters->/////////////////////////////////////////

    public function getViews()
    {
        return $this->views;
    }


    public function setViews($views): void
    {
        $this->views = $views;
    }



    public function getUsers()
    {
        return $this->users;
    }


    public function setUsers($users): void
    {
        $this->users = $users;
    }


    public function getLecture()
    {
        return $this->lecture;
    }


    public function setLecture($lecture): void
    {
        $this->lecture = $lecture;
    }



    public function getId()
    {
        return $this->id;
    }


    public function getArticleName()
    {
        return $this->articleName;
    }


    public function setArticleName($articleName): void
    {
        $this->articleName = $articleName;
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function getTeacherArticle(): ?User
    {
        return $this->teacher_article;
    }


    public function setTeacherArticle(User $teacher_article): self
    {
        $this->teacher_article = $teacher_article;

        return $this;
    }

/////////////////////////<-custom functions->////////////////////////////////////////
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function  updateTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function __toString()
    {
        return $this->getArticleName();

    }






}