<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 02/05/2018
 * Time: 13:53
 */

namespace App\Controller;




use App\Entity\User;
use App\Form\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }


    /**
     * @Route("/{id}/profile", name="edit_profile")
     */
    public function editProfile(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('edit', $user);

        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('edit_profile', ['id' => $user->getId()]);
        }

        return $this->render('user_edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);

    }

}